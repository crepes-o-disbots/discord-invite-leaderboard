const { Client, GatewayIntentBits } = require('discord.js');

// Check the token & server id from the .env file
if (!process.env.TOKEN) {
    console.error('No token provided ! Exiting.');
    process.exit(1);
}

if (!process.env.SERVER_ID) {
    console.error('No server id provided ! Exiting.');
    process.exit(2);
}

// Create a client instance for our discord.js connection
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

client.on('ready', async () => {
    console.log(`Logged in as ${client.user.tag}. Ready to get the invite numbers !`);

    client.guilds.fetch({ force: true, guild: process.env.SERVER_ID }).catch(err => {
        console.error('Failed to fetch the guild ! See the error below :');
        console.error(err);
        console.error('Exiting.');
        exit(4);
    }).then(guild => {
        console.log(`Fetched guild ${guild.name} with ${guild.id} !`);
        guild.invites.fetch({ force: true, cache: false }).catch(err => {
            console.error('Failed to fetch the invites ! See the error below :');
            console.error(err);
            console.error('Exiting.');
            exit(5);
        }).then(invites => {
            // We have the collection, we need to remove the unused invites to use it
            const invitesWUse = invites.filter(invite => invite.uses !== 0 && !invite.temporary);
            handleInvites(invitesWUse);
            exit(0);
        })
    })
});

async function exit(code) {
    await client.destroy();
    process.exit(code);
}
async function handleInvites(invites) {
    if (invites.size === 0) {
        console.log('No invite with uses found.');
        return;
    }

    console.log('Invites with uses found. Sorting them...');
    const sortedInvites = invites.sort((inviteA, inviteB) => inviteB.uses - inviteA.uses);
    for (let i = 0; i < sortedInvites.size; i++) {
        const invite = sortedInvites.at(i);
        const userData = invite.inviter != null ? `${invite.inviter.displayName}(${invite.inviter.username})` : "unknown user";
        console.log(`${i + 1} - Invite code ${invite.code} by ${userData} with id ${invite.inviterId}. Used ${invite.uses} times`);
    }
}

client.login(process.env.TOKEN).catch(err => {
    console.error('Failed to login ! See the error below :');
    console.error(err);
    console.error('Exiting.');
    process.exit(3);
});

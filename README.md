# Invite leaderboard

This program fetches invites from a specific discord guild.

For a list of packages, please refer to the [package](package.json) file.

## Installation/Usage

### Requirements

- [NodeJS](https://nodejs.org/) v20.9 or higher on your system.
- A discord bot on the guild you want to fetch the invites from (see [this link](https://discord.com/developers/applications/) to create it).
- The `Manage Server` permission on the bot, this is required to fetch the invites.

### Use

1. Clone this repo on your computer

2. Inside the cloned folder, create a .env file and put the following lines with the correct values (see [this link](https://discord.com/developers/applications/) for the token and [this link](https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-) for the server_id) :

```
TOKEN=YOUR_TOKEN_HERE
SERVER_ID=YOUR_SERVERID_HERE
```

3. Launch a terminal/powershell inside the folder and run `npm i`. This will install all required dependencies to run the bot

4. Run the bot with `npm start`. This should give you the leaderboard
